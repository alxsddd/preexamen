/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.preexamen1;

/**
 *
 * @author Alexis
 */
public class Preexamen1 {

    public static void main(String[] args) {
       Recibo1 recibo1  = new Recibo1();
        
        System.out.println("Numero de contrato:"+recibo1.getNumContrato() );
        System.out.println("Fecha de pago:"+recibo1.getFecha());
        System.out.println("Nombre del empleado:"+recibo1.getNombre());
        System.out.println("Domicilio:"+recibo1.getDomicilio());
        System.out.println("Tipo de contrato:"+recibo1.getTipo());
        System.out.println("Nivel de estudios:"+recibo1.getNivel());
        System.out.println("Pago base:"+recibo1.getPagoBase());
        System.out.println("Dias trabajados:"+recibo1.getDias());
        System.out.println("----------------------------------------------------");
        System.out.println("El subototal es de:"+ recibo1.calcularSubtotal());
        System.out.println("El impuesto es de:"+ recibo1.calcularImpuesto());
        System.out.println("El total a pagar es de:"+ recibo1.calcularTotal());
        
            Recibo2 recibo2  = new Recibo2();
        
        System.out.println("Numero de contrato:"+recibo2.getNumContrato() );
        System.out.println("Fecha de pago:"+recibo2.getFecha());
        System.out.println("Nombre del empleado:"+recibo2.getNombre());
        System.out.println("Domicilio:"+recibo2.getDomicilio());
        System.out.println("Tipo de contrato:"+recibo2.getTipo());
        System.out.println("Nivel de estudios:"+recibo2.getNivel());
        System.out.println("Pago base:"+recibo2.getPagoBase());
        System.out.println("Dias trabajados:"+recibo2.getDias());
        System.out.println("----------------------------------------------------");
        System.out.println("El subototal es:"+ recibo2.calcularSubtotal());
        System.out.println("El impuesto es de:"+ recibo2.calcularImpuesto());
        System.out.println("El total a pagar es de:"+ recibo2.calcularTotal());
    }

}