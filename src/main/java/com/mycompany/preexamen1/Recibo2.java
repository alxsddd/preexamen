/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.preexamen1;

/**
 *
 * @author Alexis
 */
class Recibo2 {
     
    private int numContrato;
    private String fecha;
    private String nombre;
    private String domicilio;
    private int tipo;
    private int nivel;
    private float pagoBase;
    private float dias;
    private float subtotal;


    public Recibo2() {
        this.numContrato = 103;
        this.fecha = "23 Marzo 2019";
        this.nombre = "Maria Acosta";
        this.domicilio = "Av del Sol 1200";
        this.tipo = 2;
        this.nivel = 2;
        this.pagoBase = 700.0f;
        this.dias = 15.0f;
    }
    public Recibo2(int numContrato, String fecha, String nombre, String domicilio, int tipo, int nivel, float pagoBase, float dias) {
        this.numContrato = numContrato;
        this.fecha = fecha;
        this.nombre = nombre;
        this.domicilio = domicilio;
        this.tipo = tipo;
        this.nivel = nivel;
        this.pagoBase = pagoBase;
        this.dias = dias;
    }
    public Recibo2(Recibo2 otro) {
        this.numContrato = otro.numContrato;
        this.fecha = otro.fecha;
        this.nombre = otro.nombre;
        this.domicilio = otro.domicilio;
        this.tipo = otro.tipo;
        this.nivel = otro.nivel;
        this.pagoBase = otro.pagoBase;
        this.dias = otro.dias;
    }    

    public int getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(int numContrato) {
        this.numContrato = numContrato;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public float getPagoBase() {
        return pagoBase;
    }

    public void setPagoBase(float pagoBase) {
        this.pagoBase = pagoBase;
    }

    public float getDias() {
        return dias;
    }

    public void setDias(float dias) {
        this.dias = dias;
    }
    
    public float calcularSubtotal () {
        float subtota1   = 0.0f;
        if (this.nivel == 1) {
            subtotal  =(this.dias * (this.pagoBase + this.pagoBase * 0.20f));
        }
        if (this.nivel == 2) {
            subtotal  =(this.dias * (this.pagoBase + this.pagoBase * 0.50f));
        }
        if (this.nivel == 3) {
            subtotal  =(this.dias * (this.pagoBase + this.pagoBase));
        }
    return subtotal;
    }
    public float calcularImpuesto () {
        float impuesto = 0.0f;
        float subtotal = calcularSubtotal();
        impuesto = this.subtotal * 0.16f;
        return impuesto;
    }
    public float calcularTotal () {
        float total = 0.0f;
        float subtotal = calcularSubtotal();
        float impuesto = calcularImpuesto();
        total = subtotal-impuesto;
        return total;
    }
    }